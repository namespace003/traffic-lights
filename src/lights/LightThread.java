package lights;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class LightThread extends Thread {
    private String LightName;
    private int timer;

    Circle light;
    Color colorValues;
    int totalTImer;
    boolean Run = true;
    private int initialSleep;
    @FXML
    private Label counterLabel;

    public LightThread(String lightName, int timer, int total, Circle light, Color colorValues, int initialSleep, Label timerLabel) {
        LightName = lightName;
        this.timer = timer;
        this.light = light;
        this.colorValues = colorValues;
        totalTImer = total;
        this.initialSleep = initialSleep;
        counterLabel = timerLabel;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(initialSleep * 1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        while (Run) {
            lightOn();
            for (int i = timer; i > 0; i--) {
                System.out.println(LightName + " Changing in ");
                try {
                    final int counter = i-1;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            counterLabel.setText(""+counter + "");
                        }
                    });
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            lightOff();
            WaitForOtherLight();

        }

    }

    @Override
    public void interrupt() {
        Run = false;
        lightOff();
        super.interrupt();
    }

    private void lightOn() {
        light.setFill(colorValues);

    }

    private void lightOff() {
        light.setFill(Color.WHITE);
        System.out.println("Switching off " + LightName);
    }

    public void WaitForOtherLight() {
        try {
            Thread.sleep(1000 * (totalTImer - timer));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
