package lightSystem;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import lights.LightThread;

public class LightController {


    public TextField green_value;
    public TextField yellow_value;
    public TextField red_value;

    public Circle red;
    public Circle green;
    public Circle yellow;
    public Button start;
    public Button stop;

    public Label greenTimer;
    public Label yellowTimer;
    public Label redTimer;
//    public TextArea logs;

    private Thread lightThreadGreen;
    private Thread lightThreadYellow;
    private Thread lightThreadRed;

    public LightController() {
        Main.rootStage.setTitle("Traffic Controller");
    }

    public void startAction() throws InterruptedException {
        int greenValue = Integer.parseInt(green_value.getText());
        int yellowValue = Integer.parseInt(yellow_value.getText());
        int redValue = Integer.parseInt(red_value.getText());

        int total = greenValue + redValue + yellowValue;
        int initialSleep = 0;
        lightThreadGreen = new LightThread("Green", greenValue, total, green, Color.GREEN,initialSleep,greenTimer);

        initialSleep = greenValue;
        lightThreadYellow = new LightThread("Yellow", yellowValue, total, yellow, Color.YELLOW, initialSleep, yellowTimer);

        initialSleep = greenValue + yellowValue;
        lightThreadRed = new LightThread("Red", redValue, total, red, Color.RED, initialSleep, redTimer);


        lightThreadGreen.start();
        lightThreadYellow.start();
        lightThreadRed.start();

    }

    public void stopAction() {

        lightThreadGreen.interrupt();
        lightThreadYellow.interrupt();
        lightThreadRed.interrupt();
    }
}
